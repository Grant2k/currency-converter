import React from 'react';
import './app-header.css';

const AppHeader = () => {
    return (
        <h1>Currency Converter</h1>
    );
};

export default AppHeader;