import React from 'react';
import './currency-title.css';

const CurrencyTitle = ({firstCurrencyValue, secondCurrencyValue, selectedCurrencyLabel}) => {

        return (
            <h3>{firstCurrencyValue} UAH equals {secondCurrencyValue} {selectedCurrencyLabel}</h3>
        );
    };    

export default  CurrencyTitle;