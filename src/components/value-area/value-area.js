import React, { Component } from 'react';
import './value-area.css';

const ValueArea = ({value, onValueChange}) => {

  const onChange = (e) => {
    let val = e.target.value;
    onValueChange(val);             
  };  

  return (
    <input type="number" 
            className="form-control form-control-lg input" 
            placeholder="Enter amount" 
            onChange={onChange}
            value={value}
    />
  );
};

// export default class ValueArea extends Component {


//   onChange = (e) => {
//     let val = e.target.value;
//     this.props.onValueChange(val);             
//   };   
  
//   render() {               
//     return (
//         <input type="number" 
//                 className="form-control form-control-lg input" 
//                 placeholder="Enter amount" 
//                 onChange={this.onChange}
//                 value={this.props.value}
//         />
//     );
//   }    
// };

export default ValueArea;
