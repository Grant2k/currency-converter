import React from 'react';

import './currency-area.css';

const CurrencyArea = ({currencyOptions, selectedCurrency, onCurrencyChange}) => {

    const onChange = (e) => {
        const cur = e.target.value;
        onCurrencyChange(cur);
    };    
    
        return (
            <select 
              className="form-control form-control-lg select"
              onChange={onChange}
              value={selectedCurrency}
            >
                {currencyOptions.map((opt) => {
                    let value = opt.value;
                    return <option key={value} value={value}>{opt.label}</option>
                })}
            </select>
        );

};

export default CurrencyArea;