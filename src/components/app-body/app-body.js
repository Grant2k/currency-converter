import React, { Component } from 'react';
import AppHeader from '../app-header';
import CurrencyTitle from '../currency-title';
import ValueArea from '../value-area';
import CurrencyArea from '../currency-area';
import CurrencyService from '../../services/currency-service';

import './app-body.css';

class AppBody extends Component {

    state = {
        firstCurrencyValue: 1,
        secondCurrencyValue: null,
        selectedCurrency: "",
        readyToRender: false,
        currencyOptions: [],
        selectedCurrencyLabel: ''
    };

    currencyService = new CurrencyService();

    async componentDidMount() {
        const currencies = await this.currencyService.getAllCurrency();

        let options = [];

        for (let currency of currencies) {
            const label = `${currency.txt}, ${currency.cc}`;
            const option = {value: currency.cc, label};
            options.push(option);
        };

        const firstCurrency = options[0];

        let selectedCurrency = firstCurrency.value;

        let currencyData = await this.getCurrencyData(selectedCurrency);    

        let secondCurrencyValue = this.calculateSecondCurrencyValue(currencyData.rate);

        this.setState({
            currencyOptions: options,
            readyToRender: true,
            selectedCurrency,
            selectedCurrencyLabel: currencyData.currName,
            secondCurrencyValue

        });
    };

    async getCurrencyData(value) {    
        let currencyData = await this.currencyService.getAnyCurrency(value);
        
        return {
            currName: currencyData[0].cc,
            rate: currencyData[0].rate
        }
    };

    calculateSecondCurrencyValue = (rate) => {
        const convertRate = 1/rate;
        const roundOffRate = Math.round(convertRate*1000)/1000;
        return roundOffRate;
    };

    onCurrencyChange = async (value) => {
        let currencyData = await this.getCurrencyData(value);    
        let secondCurrencyValue = this.calculateSecondCurrencyValue(currencyData.rate);
        
        this.setState({
            selectedCurrency: value,
            selectedCurrencyLabel: currencyData.currName,
            secondCurrencyValue: this.state.firstCurrencyValue*secondCurrencyValue

        });
    };

    onValueChange = async (value, isFirstValue=false) => {

        let selectedCurrency = this.state.selectedCurrency;

        let currencyData = await this.getCurrencyData(selectedCurrency);

        if (isFirstValue) {
  
            let secondCurrencyValue = this.calculateSecondCurrencyValue(currencyData.rate);
            secondCurrencyValue=secondCurrencyValue*value;

            this.setState({
                firstCurrencyValue: value,
                secondCurrencyValue
            });    
        } else {

            let firstCurrencyValue = value*currencyData.rate

            this.setState({
                firstCurrencyValue,
                secondCurrencyValue: value
            }); 
        }


    };

   render() {
    const {
        firstCurrencyValue,
        secondCurrencyValue,
        selectedCurrency,
        readyToRender,
        currencyOptions,
        selectedCurrencyLabel
    } = this.state;   

    if(!readyToRender) return null;
    
    return (
        <div className="app">
            <AppHeader />

            <CurrencyTitle 
                firstCurrencyValue={firstCurrencyValue}
                secondCurrencyValue={secondCurrencyValue}
                selectedCurrencyLabel={selectedCurrencyLabel} 
            />

            <span className="container">
                <span>
                    <ValueArea 
                        onValueChange={(val) => this.onValueChange(val, true)}
                        value={firstCurrencyValue}
                        
                    />
                    <ValueArea                       
                        onValueChange={this.onValueChange}
                        value={secondCurrencyValue}
                        
                    />
                </span>
                <span>
                    <select style={{width: "300px", margin: "5px"}} className="form-control form-control-lg select">
                        <option>UAH</option>
                    </select>
                    <CurrencyArea 
                        currencyOptions={currencyOptions}
                        selectedCurrency={selectedCurrency}
                        onCurrencyChange={this.onCurrencyChange} />
                </span>
            </span>
        </div>
    );
   } 
};

export default AppBody;