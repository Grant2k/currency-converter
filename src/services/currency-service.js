
export default class CurrencyService {
  _apiBase = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?';

  async getResource(url) {
    const res = await fetch(`${this._apiBase}${url}`);
    if (!res.ok) {
      throw new Error(`could not fetch ${url}, received ${res.status}`);
    }
    return await res.json();
  }

  async getAllCurrency() {
    const res = await this.getResource(`json`);
    return res;
  };

  async getAnyCurrency(id) {
    const res = await this.getResource(`valcode=${id}&json`);
    return res;
  };
};



// const currency = new CurrencyService();

// currency.getAllCurrency().then((curr) => {
//   curr.forEach((c) => {
//     console.log(`${c.txt}, ${c.cc}`);
//   })
// });

// currency.getAnyCurrency("amd").then((c) => {
//   console.log(c[0].rate);
// });

