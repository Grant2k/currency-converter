import CurrencyService from '/src/services/';

const CurrencyData = () => {
  const currencyService = new CurrencyService();
  const currencies = currencyService.getAllCurrency();
  let options = [];
  for (let currency of currencies) {
      const label = `${currency.txt}, ${currency.cc}`
      const option = {value: currency.cc, label};
      options.push(option);
  }
}

export default CurrencyData;